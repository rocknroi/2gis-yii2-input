# Yii2 2gis map input widget

## Description!
2gis map for YII2, replace input on map

## Install
```
php composer.phar require rocknroi/2gis-yii2-input
```
### Minimal example
~~~php
echo $form->field($model, 'coordinates')->widget('rocknroi\yii2\map2gis\widgets\MapInputWidget');
~~~

### Extended example
~~~php
echo $form->field($model, 'coordinates')->widget(
    'rocknroi\yii2\widgets\MapInputWidget',
    [
        // Initial map center latitude. Used only when the input has no value.
        // Otherwise the input value latitude will be used as map center.
        // Defaults to 0.
        'latitude' => 42,

        // Initial map center longitude. Used only when the input has no value.
        // Otherwise the input value longitude will be used as map center.
        // Defaults to 0.
        'longitude' => 42,

        // Initial map zoom.
        // Defaults to 0.
        'zoom' => 12,

        // Map container width.
        // Defaults to '100%'.
        'width' => '420px',

        // Map container height.
        // Defaults to '300px'.
        'height' => '420px',
    ]
);
~~~
