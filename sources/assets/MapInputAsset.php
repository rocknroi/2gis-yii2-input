<?php

namespace rocknroi\yii2\map2gis\assets;

class MapInputAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@rocknroi/2gis-yii2-input/sources/web';

    public $depends =
        [
            'yii\web\JqueryAsset',
        ];

    public $jsOptions =
        [
            'position' => \yii\web\View::POS_HEAD,
        ];

    public function __construct($config = [])
    {
        $this->js[] = $this->getGoogleMapScriptUrl();
        $this->js[] = 'js/js.js';
        $this->css[] = 'css/css.css';

        parent::__construct($config);
    }

    private function getGoogleMapScriptUrl()
    {
        $scriptUrl = "https://maps.api.2gis.ru/2.0/loader.js?pkg=full";
        return $scriptUrl;
    }
}
