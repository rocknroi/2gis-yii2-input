DG.then(function () {

    DGmarker = {};
    DGmap = {};
    $.map($('.rocknroi-2gis-input-widget-canvas'), function (elem) {
        var elemM = $(elem).parent();

        var latitude = elemM.attr('data-latitude');
        var longitude = elemM.attr('data-longitude');
        var zoom = elemM.attr('data-zoom');
        var id = elemM.attr('id');

        var elem2gis = $('rocknroi-2gis-input-widget-canvas_' + id);
        var locationInfo = $('#Val' + id);

        elem2gis.css('width', elemM.css('width'));
        elem2gis.css('height', elemM.css('height'));

        DGmap[id] = DG.map('rocknroi-2gis-input-widget-canvas_' + id, {
            'center': [latitude, longitude],
            'zoom': zoom
        });

        DGmarker[id] = DG.marker([latitude, longitude], {
            draggable: true
        }).addTo(DGmap[id]);


        DGmap[id].on('click', function (e) {
            var id = e.target._container.id.split('_')[1];
            var lat = e.latlng.lat,
                lng = e.latlng.lng;
            DGmarker[id].setLatLng(DG.latLng(lat, lng));
            locationInfo.val(lat + ',' + lng);
        });
    });


});