<?php

namespace rocknroi\yii2\map2gis\widgets;

use Yii;

class MapInputWidget extends \yii\widgets\InputWidget
{
    public $latitude = 0;
    public $longitude = 0;
    public $zoom = 12;
    public $width = '100%';
    public $height = '300px';


    public function run()
    {
        Yii::setAlias('@rocknroi','@vendor/rocknroi');

        return $this->render(
            'MapInputWidget',
            [
                'id' => $this->getId(),
                'model' => $this->model,
                'attribute' => $this->attribute,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
                'zoom' => $this->zoom,
                'width' => $this->width,
                'height' => $this->height,
            ]
        );
    }
}
