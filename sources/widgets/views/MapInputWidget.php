<?php

use yii\helpers\Html;
use yii\helpers\Url;

// Register asset bundle
rocknroi\yii2\map2gis\assets\MapInputAsset::register($this);
$id = 'mapElem' . $model->id;
// [BEGIN] - Map input widget container
echo Html::beginTag(
    'div',
    [
        'class' => 'rocknroi-2gis-input-widget',
        'style' => "width: $width; height: $height;",
        'id' => $id,
        'data' =>
        [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'zoom' => $zoom,
            'pattern' => $pattern,
            'map-type' => $mapType,
            'animate-marker' => $animateMarker,
            'align-map-center' => $alignMapCenter,
            'enable-search-bar' => $enableSearchBar,
        ],
    ]
);

    // The actual hidden input
    echo Html::activeHiddenInput(
        $model,
        $attribute,
        [
            'id' => 'Val' . $id,
            'class' => 'rocknroi-2gis-input-widget-input',
        ]
    );

    // Search bar input
    echo Html::input(
        'text',
        null,
        null,
        [
            'class' => 'rocknroi-2gis-input-widget-search-bar',
        ]
    );

    // Map canvas
    echo Html::tag(
        'div',
        '',
        [
            'id' => 'rocknroi-2gis-input-widget-canvas_' . $id,
            'class' => 'rocknroi-2gis-input-widget-canvas',
            'style' => "width: $width; height: $height;",
        ]
    );

// [END] - Map input widget container
echo Html::endTag('div');
